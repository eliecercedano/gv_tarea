/*********************************************************/
/*                Instalación                            */
/*********************************************************/
/*
  Autor  : Eliecer Cedano
  Web    : http://cedano.com.ve
  Email  : eliecercedano@gmail.com
  Version: 1.0
  Fecha  : 02 de julio de 2017
*/

/*
 * Requerimientos
 * 
 
 Laravel 5.4, Mysql.

/*
 * Pasos para ejecutar el sistema por primera vez:
 * 

Descomprima el archivo Laravel-Modulo01-Tarea01-Eliecer-Cedano.zip

Ingrese desde la consola a la carpeta Laravel-Modulo01-Tarea01-Eliecer-Cedano

Configure los parametros de acceso a la Base de Datos en el archivo .env

En mysql: crear la base de datos con el nombre configurado en el archivo .env 
		CREATE DATABASE `gv_tarea` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

Ejecutar comando para crear las tablas a utilizar y agregar los usuarios por defecto (21000000 - yiyi, 22000000 - camila)::
		php artisan migrate --seed

Por último ejecutemos el comando para servir el proyecto:
		php artisan serve

Ya puede disfrutar del sistema colocando en el browser la url http://localhost:8000

   
 * 
 */
