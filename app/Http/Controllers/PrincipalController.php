<?php
/****************************************************************************/
/*                   PrincipalController.php                                */
/*                   basado en Laravel Framework 5 para php                 */
/****************************************************************************/
/*
  Autor  : Edgar Gonzalez
  Web    : http://egonzale.org
  Email  : egonzale@ucla.edu.ve
  Laravel: version 5
  Fecha  : 14 de septiembre del 2016
  Documentacion: http://docs.sencha.com/extjs/6.2.0/
  Para ejecutar el server:
  - Abrir un terminal
  - Ir al directorio: cd c:\wamp\www\Laravel5-ExtjsBasico6
  - Ejecutar: php artisan serve --host=192.168.1.100
  Para ejecutarlo en el navagador: http://192.168.1.100:8000/ejemplo38
  Test de Ajax: 192.168.1.100:8000/ejemplo38crud?funcion=buscar&usuario=21000000&clave=yiyi

  Por el error: Middleware\VerifyCsrfToken.php line 67" ondblclick="var
  Crear una variable _token global  y usarla o pasarlas en los posts o gets de Ext.Ajax.Request, ver ejemplo38.js
  Basado en: http://stackoverflow.com/questions/30257229/laravel-post-token-missmatch-exception-file-upload
  Equivale en RoR para cada controlador: skip_before_filter :verify_authenticity_token
  O tambien agregar excepciones del URI en el archivo: VerifyCsrfToken.php que se encuentra en: app/Http/Middleware
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Usuarios;

use View;

use Fpdf;

/* En Windows */
//require_once app_path() . '\Libraries\Libreria.php';
/* En Linux */
require_once app_path() . '/Libraries/Libreria.php';

class PrincipalController extends Controller
{
   private $usuario;
   private $clave;
	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      // Se asigna la localizacion
      $obj = new Changelocalizacion();
      $obj->update();
      if (session()->exists('usuario_sesion') && session()->exists('clave_sesion')) {
       // Se asigna la localizacion
       $locale = session()->get('localizacion_sesion');
       \App::setLocale($locale);
       //Se asigna a las variables de la clase
       $this->usuario = session()->get('usuario_sesion');
       $this->clave   = session()->get('clave_sesion');
       return View::make('pages.principal', ['usuario' => $this->usuario]);
      } else {
        return redirect('/error?msgerror="'. __('messages.msg01') .'"');
      }
       
    }
}
?>