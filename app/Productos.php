<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/* En Windows */
//require_once app_path() . '\Libraries\Libreria.php';
/* En Linux */
//require_once app_path() . '/Libraries/Libreria.php';
use app\Libreria;

class Productos extends Model
{
 function Buscar($Datos)
 {
  $codigo = $Datos->getV(0);
  $Valor = 0;
  $productoRecord = Productos::where('codigo', '=', $codigo)->first();
  if ($productoRecord!="")
  { 
   $Datos->setV(1,$productoRecord->nombre);
   $Datos->setV(2,$productoRecord->cantidad);
   $Valor = 1;
  }
  else
  { 
   $Valor = 0;
  }
  return $Valor;
 }

 function BuscarTodos($Datos)
 {
  $Valor = 0;
  $registros = Productos::all();
  $j=0;
  foreach ($registros as $registro) {
    if ($j==0) { $Valor = 1; }
    $Datos->setV($j,0,$registro->codigo);
    $Datos->setV($j,1,$registro->nombre);
    $Datos->setV($j,2,$registro->cantidad);
    $j=$j+1;
  }
  return $Valor;
 } 

  function Grabar($Datos)
 {
  $codigo   = $Datos->getV(0);
  $nombre   = $Datos->getV(1);
  $cantidad = $Datos->getV(2);
  $Valor    = 0;
  $productoRecord = Productos::where('codigo', '=', $codigo)->first();
  if ($productoRecord!="")
  { 
   $productoRecord->nombre   = $nombre;
   $productoRecord->cantidad = $cantidad;
   $productoRecord->save();
   $Valor = 1;
  }
  else
  { 
   $objproducto = new productos;
   $objproducto->codigo   = $codigo;
   $objproducto->nombre   = $nombre;
   $objproducto->cantidad = $cantidad;
   $objproducto->save();
   $Valor = 1;
  }
  return $Valor;
 }

 function Eliminar($Datos)
 {
  $codigo   = $Datos->getV(0);
  $nombre   = $Datos->getV(1);
  $cantidad = 0;
  $productoRecord = Productos::where('codigo', '=', $codigo)->first();
  if ($productoRecord!="")
  { 
   $productoRecord->delete();
   $Valor = 1;
  }
  else
  { 
   $Valor = 0;
  }
  return $Valor;
 }  
}
