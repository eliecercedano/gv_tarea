-- 
-- Database: `gv_tarea`
-- 
CREATE DATABASE `gv_tarea` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gv_tarea`;

-- --------------------------------------------------------

-- 
-- Table structure for table `productos`
-- 

CREATE TABLE `productos` (
  `id` int(11) NOT NULL auto_increment,
  `codigo` varchar(15) NOT NULL,
  `nombre` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

-- 
-- Table structure for table `usuarios`
-- 

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL auto_increment,
  `usuario` varchar(10) NOT NULL default '',
  `clave` varchar(10) NOT NULL default '',
  `nivel` char(2) NOT NULL default '',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `usuarios`
-- 

INSERT INTO `usuarios` (`id`, `usuario`, `clave`, `nivel`, `created_at`, `updated_at`) VALUES (1, '21000000', 'yiyi', '1', '2017-06-30 08:25:45', '2017-06-30 08:25:45'),
(5, '22000000', 'camila', '2', '2017-06-30 08:25:45', '2017-06-30 08:25:45');
