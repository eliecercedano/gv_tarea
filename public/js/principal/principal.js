/*********************************************************/
/*                producto.js                           */
/*********************************************************/
/*
  Autor  : Edgar Gonzalez
  Web    : http://egonzale.org
  Email  : egonzale@ucla.edu.ve
  Version: 1.0 en javascript 1.2
  Fecha  : 14 de septiembre del 2016
*/

/*
 * Comentario:
 * 
 
  Actualizacion de datos con AJAX/AJAJ
  
  En este ejemplo vamos hacer un formulario de actualizacion de datos
  o un CRUD.
  
  Para ello, nos comunicamos con el servidor a traves de una pagina
  servidora llamada: producto_controlador.php. Esta pagina, recibira los
  parametros vía post usando este javascript que hara el request a esta
  pagina servidora usando Ext.Ajax.request y Ext.JSON.decode
   
 * 
 */

var url_ajax = 'accesocrud';
//variable global para las acciones de los botones
var funcion = null;
var validado = false;

//funcion que valida la entrada y carga la variable global funcion
function Validar(opcion) {
 funcion = opcion;
 if (funcion=="cerrar" || funcion=="ayuda" || funcion=="usuario" || funcion=="producto") {
  validado = false;
  acciones();
  return;
 } 
}

function acciones() {

 switch (funcion)
 {
  case "usuario" :
  document.getElementsByName("main2")[0].innerHTML="<div align='center'><img src='../../images/loading.gif'/></div>";
  	Ext.getBody().mask(appLocale.msg04);
    Ext.onReady(function() {
	 Ext.Ajax.request({
		url: url_ajax,
	    //Enviando los parametros a la pagina servidora
	    params: {
	     funcion: 'usuario',
	     _token: _token
	    },
		callback: function(options, success, response){
			Ext.getBody().unmask();
            datos=Ext.JSON.decode(response.responseText);
            if (datos.exito=='true') {
             /*
             Ext.MessageBox.show({
		   		     	 title: appLocale.title02,
		   		     	 msg: datos.msg,
		   		     	 //msg: response.responseText,
		   		     	 buttons: Ext.MessageBox.OK,
		   		     	 //buttons: Ext.MessageBox.YESNO,
		   		     	 icon: Ext.MessageBox.INFO,
		   		     	 fn: function (id, value, opt) {
		   		     	  if (id === 'ok') {
		   		     	  	location.href=home_url+datos.url;
		   		     	  }
		   		     	 }  
		   	 });
		   	 */
		   	 location.href=home_url+datos.url;
            }
            else {
             Ext.MessageBox.show({
   		     	 title: appLocale.title01,
   		     	 msg: datos.msg,
   		     	 //msg: response.responseText,
   		     	 buttons: Ext.MessageBox.OK,
   		     	 //buttons: Ext.MessageBox.YESNO,
   		     	 icon: Ext.MessageBox.ERROR,
   		     	 fn: function (id, value, opt) {
   		     	  if (id === 'ok') {
   		     	  }
   		     	 }  
   			 });
   			 document.getElementsByName("main2")[0].innerHTML="";
            }
		},
		failure: function(response, options) {
		    Ext.getBody().unmask();
			Ext.MessageBox.show({
		     	 title: appLocale.title01,
		     	 msg: appLocale.msg98+response.status+"<br/>"+
		     	      appLocale.msg99+response.statusText,
		     	 buttons: Ext.MessageBox.OK,
		     	 //buttons: Ext.MessageBox.YESNO,
		     	 icon: Ext.MessageBox.ERROR,
		     	 fn: function (id, value, opt) {
		     	  if (id === 'ok') {
		     	  }
		     	 }  
			});
			document.getElementsByName("main2")[0].innerHTML="";
		},		
		timeout: 60000 //60 segundos de espera (por defecto es 30)
	 });	
  });
  break

  case "producto" :
  	document.getElementsByName("main2")[0].innerHTML="<div align='center'><img src='../../images/loading.gif'/></div>";
  	Ext.getBody().mask(appLocale.msg04);
    Ext.onReady(function() {
	 Ext.Ajax.request({
		url: url_ajax,
	    //Enviando los parametros a la pagina servidora
	    params: {
	     funcion: 'producto',
	     _token: _token
	    },
		callback: function(options, success, response){
			Ext.getBody().unmask();
            datos=Ext.JSON.decode(response.responseText);
            if (datos.exito=='true') {
             /*
             Ext.MessageBox.show({
		   		     	 title: appLocale.title02,
		   		     	 msg: datos.msg,
		   		     	 //msg: response.responseText,
		   		     	 buttons: Ext.MessageBox.OK,
		   		     	 //buttons: Ext.MessageBox.YESNO,
		   		     	 icon: Ext.MessageBox.INFO,
		   		     	 fn: function (id, value, opt) {
		   		     	  if (id === 'ok') {
		   		     	  	location.href=home_url+datos.url;
		   		     	  }
		   		     	 }  
		   	 });
		   	 */
		   	 location.href=home_url+datos.url;
            }
            else {
             Ext.MessageBox.show({
   		     	 title: appLocale.title01,
   		     	 msg: datos.msg,
   		     	 //msg: response.responseText,
   		     	 buttons: Ext.MessageBox.OK,
   		     	 //buttons: Ext.MessageBox.YESNO,
   		     	 icon: Ext.MessageBox.ERROR,
   		     	 fn: function (id, value, opt) {
   		     	  if (id === 'ok') {
   		     	  }
   		     	 }  
   			 });
   			 document.getElementsByName("main2")[0].innerHTML="";
            }
		},
		failure: function(response, options) {
		    Ext.getBody().unmask();
			Ext.MessageBox.show({
		     	 title: appLocale.title01,
		     	 msg: appLocale.msg98+response.status+"<br/>"+
		     	      appLocale.msg99+response.statusText,
		     	 buttons: Ext.MessageBox.OK,
		     	 //buttons: Ext.MessageBox.YESNO,
		     	 icon: Ext.MessageBox.ERROR,
		     	 fn: function (id, value, opt) {
		     	  if (id === 'ok') {
		     	  }
		     	 }  
			});
			document.getElementsByName("main2")[0].innerHTML="";
		},		
		timeout: 60000 //60 segundos de espera (por defecto es 30)
	 });	
  });
  break

  case "ayuda":  	
  	document.getElementsByName("main2")[0].innerHTML="<div align='center'><img src='../../images/loading.gif'/></div>";
  	location.href=home_url+funcion;  	
  break
  
  	case "cerrar":
  		Ext.getBody().mask(appLocale.msg07);
	   Ext.onReady(function() {
	   	document.getElementsByName("main2")[0].innerHTML="<div align='center'><img src='../../images/loading.gif'/></div>";
		 Ext.Ajax.request({
			url: url_ajax,
		    //Enviando los parametros a la pagina servidora
		    params: {
		     funcion: 'cerrar',
		     _token: _token
		    },
			callback: function(options, success, response){
				Ext.getBody().unmask();
	            datos=Ext.JSON.decode(response.responseText);
	            if (datos.exito=='true') {
	             Ext.MessageBox.show({
	   		     	 title: appLocale.title02,
	   		     	 msg: datos.msg+datos.url,
	   		     	 //msg: response.responseText,
	   		     	 buttons: Ext.MessageBox.OK,
	   		     	 //buttons: Ext.MessageBox.YESNO,
	   		     	 icon: Ext.MessageBox.INFO,
	   		     	 fn: function (id, value, opt) {
	   		     	  if (id === 'ok') {
	   		     	   location.href=home_url+datos.url;
	   		     	  }
	   		     	 }  
	   			 });
	            }
	            else {
	             Ext.MessageBox.show({
	   		     	 title: appLocale.title01,
	   		     	 msg: datos.msg,
	   		     	 //msg: response.responseText,
	   		     	 buttons: Ext.MessageBox.OK,
	   		     	 //buttons: Ext.MessageBox.YESNO,
	   		     	 icon: Ext.MessageBox.ERROR,
	   		     	 fn: function (id, value, opt) {
	   		     	  if (id === 'ok') {
	   		     	  }
	   		     	 }  
	   			 });
	            }
			},
			failure: function(response, options){
				Ext.getBody().unmask();
				Ext.MessageBox.show({
			     	 title: appLocale.title01,
			     	 msg: appLocale.msg98+response.status+"<br/>"+
			     	      appLocale.msg99+response.statusText,
			     	 buttons: Ext.MessageBox.OK,
			     	 //buttons: Ext.MessageBox.YESNO,
			     	 icon: Ext.MessageBox.ERROR,
			     	 fn: function (id, value, opt) {
			     	  if (id === 'ok') {
			     	  }
			     	 }  
				});
			},		
			timeout: 60000 //60 segundos de espera (por defecto es 30)
		 });	
	  });
	break;

 }
}




