<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Usuarios;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->truncate();
        DB::table('usuarios')->insert([
            ['id' => 1, 'usuario' => '21000000', 'clave' => 'yiyi', 'nivel' => '1', 'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')],
            ['id' => 2, 'usuario' => '22000000', 'clave' => 'camila', 'nivel' => '2', 'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')],
            ['id' => 3, 'usuario' => '23000000', 'clave' => '1234', 'nivel' => '1','created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')],
        ]);
        $this->command->info('Tabla usuarios actualizada!');
    }
}
