<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.acceso', ['primeravez' => 'false']);
});

Route::resource('error'    , 'ErrorController');
Route::resource('ayuda'    , 'AyudaController');

Route::resource('acceso'   , 'AccesoController');
Route::get('accesocrud'    , 'AccesoController@crud');
Route::post('accesocrud'   , 'AccesoController@crud');
Route::get('accesolocale'  , 'AccesoController@setLocalizacion');
Route::post('accesolocale' , 'AccesoController@setLocalizacion');

Route::resource('principal', 'PrincipalController');

Route::resource('usuario', 'UsuarioController');
Route::get('usuariocrud', 'UsuarioController@crud');
Route::post('usuariocrud', 'UsuarioController@crud');

Route::resource('producto', 'ProductoController');
Route::get('productocrud', 'ProductoController@crud');
Route::post('productocrud', 'ProductoController@crud');
