 @include('includes.header')
 <!-- Incluya los javascripts y css propios -->
 <script type="text/javascript" src="js/principal/principal.js"></script>
 <link rel="stylesheet" type="text/css" href="css/micss.css">

</head>
<body>
<div align="center">
<h1><?php echo Config::get('constants.version_curso'); ?></h1>

 <div id="myDiv1">
  <h2>{{ __('labels.maintitle') }}: {{ $usuario }}</h2><br>  
  <div id="main2" name="main2"></div>
  <div id="main" name="main">
    <ul id="navigationMenu">
        <li>
          <a class="home" href="#" onClick="Validar('cerrar');">
                <span>{{ __('labels.opt1') }}</span>
            </a>
        </li>
        
        <li>
          <a class="about" href="#" onClick="Validar('ayuda');">
                <span>{{ __('labels.opt2') }}</span>
            </a>
        </li>
        
        <li>
           <a class="services" href="#" onClick="Validar('usuario');">
                <span>{{ __('labels.opt3') }}</span>
             </a>
        </li>
        
        <li>
          <a class="portfolio" href="#" onClick="Validar('producto');">
                <span>{{ __('labels.opt4') }}</span>
            </a>
        </li>
        
        <li>
          <a class="contact" href="#" onClick="location.href='mailto:eliecercedano@gmail.com';">
                <span>{{ __('labels.opt5') }}</span>
            </a>
        </li>
</ul>
    
</div>

 </div>

 @include('includes.footer')

</div>
</body>
</body>
</html>
