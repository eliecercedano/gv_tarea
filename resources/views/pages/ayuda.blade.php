 @include('includes.header')
 <!-- Incluya los javascripts y css propios -->
 <script type="text/javascript" src="js/ayuda/ayuda.js"></script>
 <link rel="stylesheet" type="text/css" href="css/micss.css">

</head>
<body>
<div align="center">
<h1><?php echo Config::get('constants.version_curso'); ?></h1>

 <div id="myDiv1">
  <h2>{{ __('labels.maintitle') }}: {{ $usuario }}</h2><br>  
  <div id="main">
    <ul id="navigationMenu">
        <li>
          <a class="home" href="principal">
                <span>{{ __('labels.opthlp') }}</span>
            </a>
        </li>
</ul>
    
</div>

  
 </div>
 @include('includes.footer')
</div>
</body>
</body>
</html>