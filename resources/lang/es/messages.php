<?php

// resources/lang/es/messages.php

return [
    'msg01'  => 'No autorizado para acceder: Usuarios',
    'msg02'  => 'Existe el usuario. Ver los datos',
    'msg03'  => 'No existe el usuario y/o clave',
    'msg04'  => 'Transacci&oacute;n exitosa',
    'msg05'  => 'Transacci&oacute;n NO exitosa',
    'msg06'  => 'Eliminaci&oacute;n exitosa',
    'msg07'  => 'Eliminaci&oacute;n NO exitosa',
    'msg08'  => 'Reporte generado en pdf',
    'msg09'  => 'Reporte NO generado en pdf',
    'msg10'  => 'Sesi&oacute;n Cerrada exitosamente. Llamar&aacute; al controlador: ',
    'msg11'  => 'Sesi&oacute;n NO Cerrada exitosamente',
    'msg12'  => 'Existe el usuario. Llamar&aacute; al controlador: ',
    'msg13'  => 'No existe el usuario y/o clave',
    'msg21'  => 'No autorizado para acceder: Productos',
    'msg22'  => 'Existe el producto. Ver los datos',
    'msg23'  => 'No existe el producto',
    'msg99'  => 'No autorizado para acceder a este modulo',
];