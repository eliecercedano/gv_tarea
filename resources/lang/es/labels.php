<?php

// resources/lang/es/labels.php

return [
    'user'   => 'Usuario',
    'pwd'    => 'Clave',
    'level'  => 'Nivel',
    'code'   => 'Codigo',
    'name'   => 'Nombre',
    'stock'  => 'Cantidad',
    'title1' => 'Acceso',
    'title2' => 'Actualizar Usuarios - Sesi&oacute;n iniciada por el usuario',
    'title3' => 'Cambiar idioma',
    'title4' => 'Reporte del Sistema',
    'title5' => 'Actualizar Productos - Sesi&oacute;n iniciada por el usuario',
    'maintitle' => 'Seleccione el m&oacute;dulo que desea acceder - Sesi&oacute;n iniciada por el usuario',
    'opt1'   => 'Salir',
    'opt2'   => 'Ayuda',
    'opt3'   => 'Usuarios',
    'opt4'   => 'Productos',
    'opt5'   => 'Contacto',
    'index1' => 'Seleccione el Idioma',
    'index2' => 'Ingrese su usuario y clave de acceso',
    'index3' => 'Pulse el boton enviar',
    'opthlp' => 'Regresar',
];