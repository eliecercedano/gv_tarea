<?php

// resources/lang/es/buttons.php

return [
    'find'   => 'Buscar',
    'save'   => 'Grabar',
    'clear'  => 'Limpiar',
    'delete' => 'Eliminar',
    'print'  => 'Imprimir',
    'close'  => 'Regresar',
    'send'   => 'Enviar',
    'cancel' => 'Cancelar',
    'change' => 'Cambiar'
];