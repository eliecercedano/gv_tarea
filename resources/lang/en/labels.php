<?php

// resources/lang/en/labels.php

return [
    'user'   => 'User',
    'pwd'    => 'Password',
    'level'  => 'Level',
    'code'   => 'Code',
    'name'   => 'Name',
    'stock'  => 'Stock',
    'title1' => 'Login',
    'title2' => 'Update Users - User logued',
    'title3' => 'Change language',
    'title4' => 'System Users',
    'title5' => 'Update Products - User logued',
    'maintitle' => 'Select option from menu - User logued',
    'opt1'   => 'Exit',
    'opt2'   => 'Help',
    'opt3'   => 'Users',
    'opt4'   => 'Products',
    'opt5'   => 'Contact',
    'index1' => 'Select Language',
    'index2' => 'Enter your username and password',
    'index3' => 'Press the send button',
    'opthlp' => 'Return',
];