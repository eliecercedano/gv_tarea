<?php

// resources/lang/es/buttons.php

return [
    'find'   => 'Suche',
    'save'   => 'Rekord',
    'clear'  => 'Sauber',
    'delete' => 'Entfernen',
    'print'  => 'Drucken',
    'close'  => 'schließen',
    'send'   => 'Schließen',
    'cancel' => 'Stornieren',
    'change' => 'Veränderung'
];