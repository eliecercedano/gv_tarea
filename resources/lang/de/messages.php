<?php

// resources/lang/es/messages.php

return [
    'msg01'  => 'Keine Zugriffsberechtigung : Benutzer',
    'msg02'  => 'Dort wird der Benutzer. anzeigen von Daten',
    'msg03'  => 'Es gibt keinen Benutzername oder Passwort',
    'msg04'  => 'erfolgreiche Transaktion',
    'msg05'  => 'keine erfolgreiche Transaktion',
    'msg06'  => 'erfolgreiche Entfernung',
    'msg07'  => 'erfolglos Entfernung',
    'msg08'  => 'Generiert Bericht PDF',
    'msg09'  => 'Unzumutbar nicht im PDF erzeugt',
    'msg10'  => 'Erfolgreich abgeschlossen Sitzung.',
    'msg11'  => 'Session nicht erfolgreich abgeschlossen.',
    'msg12'  => 'Der Benutzer vorhanden ist',
    'msg13'  => 'Es gibt keinen Benutzername oder Passwort.',
    'msg21'  => 'Keine Zugriffsberechtigung: Produkte',
    'msg22'  => 'Es ist das Produkt. anzeigen von Daten',
    'msg23'  => 'Produkt wurde nicht gefunden',
    'msg99'  => 'Keine Zugriffsberechtigung',
];