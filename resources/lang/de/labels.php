<?php

// resources/lang/es/labels.php

return [
    'user'   => 'Benutzer',
    'pwd'    => 'Schlüssel',
    'level'  => 'Ebene',
    'code'   => 'Gesetzbuch',
    'name'   => 'Name',
    'stock'  => 'Quantität',
    'title1' => 'Zugang',
    'title2' => 'Modernisierung Benutzer - Gespeichert Benutzer',
    'title3' => 'Sprache ändern',
    'title4' => 'Systembenutzer',
    'title5' => 'Aktualisierung Produkte - Gespeichert Benutzer',
    'maintitle' => 'Wählen Sie das Modul Sie zugreifen möchten - Gespeichert Benutzer',
    'opt1'   => 'Ausfahrt',
    'opt2'   => 'Hilfe',
    'opt3'   => 'Benutzer',
    'opt4'   => 'produzieren',
    'opt5'   => 'Kontakt',
    'index1' => 'wählen Sie die Sprache',
    'index2' => 'Geben Sie Ihren Benutzernamen und das Passwort',
    'index3' => 'Drücken Sie die Sendetaste',
    'opthlp' => 'Rückkehr',
];